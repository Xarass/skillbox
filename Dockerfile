FROM node:alpine
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile && yarn cache clean
COPY . ./
RUN cp .env.example.js ./.env.js && yarn build && yarn cache clean
EXPOSE 3000
CMD yarn start
